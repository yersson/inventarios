
package electrodomesticos;

import java.util.*;
import java.io.*;


public abstract class Electronicos {

    static Scanner entrada = new Scanner(System.in);
    static String nombre;
    static double precio;
    static int opcion, stock, codigo;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Electro Producto;
        Vector productos = new Vector();
            Radio Gra = new Radio();
        do {
            System.out.print("MENU"
                    + "1)Registro\n"
                    + "2)Eliminar\n"
                    + "3)Buscar\n"
                    + "5)electrodoméstico\n"
                    + "6)Salir\n\n -->");

            opcion = entrada.nextInt();
         

            switch (opcion) {
                case 1:
                    System.out.print("código: ");
                    codigo = entrada.nextInt();

                    System.out.println("Nombre");
                    nombre = entrada.next();
                    System.out.println("Precio");
                    precio = entrada.nextDouble();
                    System.out.println("Número");
                    stock = entrada.nextInt();
                    Producto = busqueda(codigo, productos);

                    if (Producto == null) {
                        productos.addElement(new Electro(codigo, nombre, precio, stock));
                        System.out.print("\n\n***Ingreso Exitoso");
                    } else {
                        System.err.print("ya existe este producto");
                    }

                    break;

                case 2:
                    System.out.print("Ingreso de código: ");
                    codigo = entrada.nextInt();
                    Producto = busqueda(codigo, productos);
                    if (Producto != null) {
                        if (Producto.getStock() > 0) {
                            Producto.setStock(Producto.getStock() - 1);
                            System.out.print("Eliminación con Venta Exitosa");
                            Producto.mostrarResultado();
                        } else {
                            System.out.print("Sin resultados de Stock");
                            break;
                        }
                    } else {
                        System.out.print("sin exito");
                    }

                    break;

                case 3:
                    System.out.print("Ingreso de código: ");
                    codigo = entrada.nextInt();
                    Producto = busqueda(codigo, productos);
                    if (Producto != null) {
                        Producto.mostrarResultado();
                    } else {
                        System.out.print("Busqueda de información sin exito");
                    }
                    break;

               
                case 5:                   
                    Gra.Encender();
                    break;
                case 6:

                    System.out.println("Vuelva pronto...");
                    break;

                default:
                    System.err.print("Opción incorrecta!, intente el proceso nuevamente\n");
                    break;
            }
        } while (opcion != 6);
    }

    static Electro busqueda(int codigo, Vector productos) {
        boolean band = false;
        Electro p, retornP = null;
        for (int x = 0; x < productos.size(); x++) {
            p = (Electro) productos.elementAt(x);
            if (p.getCodigo() == codigo) {
                retornP = p;
            }
        }
        return retornP;
    }
}

class Electro {

    int codigo, stock;
    double presio;
    String nombre;

    Electro(int codigo, String nombre, double precio, int stock) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.presio = precio;
        this.stock = stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getStock() {
        return stock;
    }

    public void mostrarResultado() {
        System.out.println("-------Resultado-------");
        System.out.println("\nNombre = " + nombre + "\nPrecio = $" + presio + "\nStock = " + stock + "/n");
    }

}
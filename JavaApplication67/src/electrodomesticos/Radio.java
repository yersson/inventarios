
package electrodomesticos;

import java.util.*;

public class Radio {

    private int volumen;
    private String pantalla;
    private int opcion, opcionGrabadora, encendido, apagado;
    private int cassett, radio, cd;
    private Scanner leer = new Scanner(System.in);

    

    public void Encender() {
        do {
            System.out.println("1) Encender Grabadora\n");
            encendido = leer.nextInt();
            if (encendido == 1) {
                System.out.println("***Grabadora Encendida***");
            } else {
                Encender();
            }
        } while (encendido != 1);

    }

   
    public int getVolumen() {
        return volumen;
    }

    public void setVolumen(int volumen) {
        this.volumen = volumen;
    }

    public String getPantalla() {
        return pantalla;
    }

    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }

    public int getEncendido() {
        return encendido;
    }

    public void setEncendido(int encendido) {
        this.encendido = encendido;
    }

    public int getApagado() {
        return apagado;
    }

    
    public void setApagado(int apagado) {
        this.apagado = apagado;
    }

    public int getCassett() {
        return cassett;
    }

    public void setCassett(int cassett) {
        this.cassett = cassett;
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public int getCd() {
        return cd;
    }

    public void setCd(int cd) {
        this.cd = cd;
    }

    public Scanner getLeer() {
        return leer;
    }

    public void setLeer(Scanner leer) {
        this.leer = leer;
    }

}
